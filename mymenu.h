// --- myMenuFolder -----------------------------------------------------------

class myMenuFolder : public cOsdMenu {
private:
  cNestedItemList *nestedItemList;
  cList<cNestedItem> *list;
  cString dir;
  cOsdItem *firstFolder;
  bool editing;
  int helpKeys;
  int whatToDo;
  void SetHelpKeys(void);
  void Set(const char *CurrentFolder = NULL);
  void DescendPath(const char *Path);
  eOSState SetFolder(void);
  eOSState Select(bool Open);
  eOSState New(void);
  eOSState Delete(void);
  eOSState Edit(void);
  myMenuFolder(const char *Title, cList<cNestedItem> *List, cNestedItemList *NestedItemList, const char *Dir, const char *Path = NULL, int WhatToDo = 0);
public:
  myMenuFolder(const char *Title, cNestedItemList *NestedItemList, const char *Path = NULL, int WhatToDo = 0);
  cString GetFolder(void);
  virtual eOSState ProcessKey(eKeys Key);
  };

// --- myMenuPathEdit ---------------------------------------------------------

class myMenuPathEdit : public cOsdMenu {
private:
  cString path;
  cString oldFolder;
  char folder[PATH_MAX];
  char name[NAME_MAX];
  cMenuEditStrItem *folderItem;
  int whatTodoPath;
  const char *buttonAction;
  const char *actionCancel;
  const char *buttonFolder;
    const char *doAll;
  const char *doName;
  const char *doCopy;
  const char *doMove;
  bool extraAction;
  int pathIsInUse;
  void SetHelpKeys(void);
  eOSState SetFolder(void);
  eOSState Folder(void);
  eOSState ApplyChanges(void);
public:
  myMenuPathEdit(const char *Path, int WhatTodoPath = 0);
  virtual eOSState ProcessKey(eKeys Key);
  };

// --- myMenuRecordingEdit ----------------------------------------------------

class myMenuRecordingEdit : public cOsdMenu {
private:
  const cRecording *recording;
  cString originalFileName;
  cStateKey recordingsStateKey;
  int whatTodoRec;
  char folder[PATH_MAX];
  char name[NAME_MAX];
  const char *menuRename[4];
  char title[NAME_MAX];
  char shorttext[NAME_MAX];
  char title_shorttext[NAME_MAX+NAME_MAX + 3]; // 3: " - "
  char old_name[NAME_MAX];
  char start_name[NAME_MAX];
  int name_preselect = 0; // default: 'Manual'
  int old_name_preselect = 0;
  int name_0_percent = false;
  bool old_name_0_percent = false;
  const char *description;
  bool description_clear = false;
  bool InfoModified = false;
  int priority;
  int lifetime;
  cMenuEditStrItem *folderItem;
  cMenuEditStrItem *nameItem;
  const char *buttonFolder;
  const char *buttonAction;
  const char *buttonExtraAction;
  const char *buttonDeleteMarks;
  const char *actionCancel;
  const char *doAll;
  const char *doName;
  const char *doCut;
  const char *doCopy;
  const char *doMove;
  const char *doRest;
  bool extraAction;
  int recordingIsInUse;
  void Set(void);
  void SetHelpKeys(void);
  bool RefreshRecording(void);
  eOSState SetFolder(void);
  eOSState Folder(void);
  eOSState Action(void);
  eOSState RemoveName(void);
  eOSState DeleteMarks(void);
  eOSState SetName(void);
#if VDRVERSNUM >= 20502 || defined EPGRENAME
  eOSState ClearEpgDescription(void);
#endif
  eOSState ApplyChanges(void);
public:
  myMenuRecordingEdit(const cRecording *Recording, int WhatTodoRec = 0);
  virtual eOSState ProcessKey(eKeys Key);
  };

// --- myMenuRecording --------------------------------------------------------
class myMenuRecording : public cOsdMenu {
private:
  const cRecording *recording;
  cString originalFileName;
  cStateKey recordingsStateKey;
  bool withButtons;
  bool deletedRecording;
  bool RefreshRecording(void);
public:
  myMenuRecording(const cRecording *Recording, bool WithButtons = false, bool DeletedRecording = false);
  virtual void Display(void);
  virtual eOSState ProcessKey(eKeys Key);
#ifdef USE_GRAPHTFT
  virtual const char* MenuKind(){return "MenuExtRecording";}
#endif
};

// --- myMenuRecordingItem ---------------------------------------------------
class myMenuRecordingItem : public cOsdItem {
private:
  const cRecording *recording;
  bool dirismoving;
  bool isdvd;
  bool ishdd;
  int level, isdirectory;
  int totalEntries, newEntries;
  char *title;
  char *name;
  const char *filename;
public:
  myMenuRecordingItem(const cRecording *Recording, int Level);
  ~myMenuRecordingItem();
  void IncrementCounter(bool New);
  const char *Name() { return name; }
  const char *FileName() { return filename; }
  int Level(void) { return level; }
  const cRecording *Recording(void) { return recording; }
  bool IsDirectory(void) { return name != NULL; }
  void SetRecording(const cRecording *Recording) { recording = Recording; }
  bool IsDVD() {return isdvd;}
  bool IsHDD() {return ishdd;}
  void SetDirIsMoving(bool moving) { dirismoving = moving; }
  bool GetDirIsMoving() {return dirismoving; }
  virtual void SetMenuItem(cSkinDisplayMenu *DisplayMenu, int Index, bool Current, bool Selectable);
};

// --- myMenuRecordings -------------------------------------------------------
class myMenuRecordings : public cOsdMenu
{
 private:
  static bool wasdvd;
  static bool washdd;
  static bool golastreplayed;
  static dev_t fsid;
  static time_t lastDiskSpaceCheck;
  static int lastFreeMB;
  cStateKey recordingsStateKey;
  const cRecordingFilter *filter;
  static cString path;
  static cString fileName;
  char *base;
  int level;
  int counter;
  int ebene;
  int helpkeys;
  bool editMenu;
  bool EditMenu;
  int timeOut;
  bool delRecMenu;
  bool deletedRec;
  bool hascutter;
  bool hasmovecopy;
  const char *buttonRed;
  const char *buttonGreen;
  const char *buttonYellow;
  const char *buttonBlue;
  void Set(bool Refresh=false);
  bool Open(bool OpenSubMenus = false);
  void SetHelpKeys(void);
  void Title();
  bool MountHDD(const char *Name);
  int MountDVD(const char *Name);
  cRecording *GetRecording(myMenuRecordingItem *Item);
  eOSState Play(void);
  eOSState Rewind(void);
  eOSState Erase(void);
  eOSState Undelete(void);
  eOSState Delete(void);
  eOSState MoveRename(int i = 0);
  eOSState Info(void);
  eOSState Sort(void);
  eOSState Details(void);
  eOSState Commands(eKeys Key = kNone);
  eOSState ChangeSorting(void);
  eOSState state;
  int FreeMB();
 protected:
  cString DirectoryName(void);
 public:
  myMenuRecordings(const char *Base = NULL, int Level = 0, bool OpenSubMenus = false, const cRecordingFilter *Filter = NULL, bool DelRecMenu = false);
  ~myMenuRecordings();
  virtual eOSState ProcessKey(eKeys Key);
  static void SetPath(const char *Path);
  static void SetRecording(const char *FileName);
#ifdef USE_GRAPHTFT
  virtual const char* MenuKind() {return"MenuExtRecordings";}
#endif
  void ForceFreeMbUpdate() {lastDiskSpaceCheck = 0;};
  void mySetCols(void);
};
