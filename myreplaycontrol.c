/*
 * See the README file for copyright information and how to reach the author.
 */

#include <vdr/interface.h>
#include <vdr/menu.h>
#include "myreplaycontrol.h"
#include "mysetup.h"

using namespace std;

myReplayControl::myReplayControl()
{
//  timesearchactive = false;
  fCallPlugin = mysetup.ReturnToPlugin;
}

myReplayControl::~myReplayControl()
{
  if (fCallPlugin) {
     mysetup.ReturnToRec = true;
     cRemote::CallPlugin("extrecmenung");
  }
}

eOSState myReplayControl::ProcessKey(eKeys Key)
{
  eOSState lastState = cReplayControl::ProcessKey(Key);
  if (lastState == osRecordings)
     lastState = osEnd;
  if (lastState == osEnd && mysetup.ReturnToPlugin)
     fCallPlugin = true;
  if (Key == kBlue || Key == kStop)
     fCallPlugin = false;
  return lastState;
}
