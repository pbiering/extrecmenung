#!/bin/sh

cd $2
echo "Done."
at now <<EOF
    rm $2/watched.vdr >/dev/null 2>&1
    if [ ! ${1} = "0" ]; then
        echo "${1}" > $2/watched.vdr
    fi
    sleep 1
    /usr/bin/svdrpsend HITK BACK BACK BACK
EOF
