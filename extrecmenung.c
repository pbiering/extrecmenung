/*
 * See the README file for copyright information and how to reach the author.
 */

#include <string>
#include <vdr/plugin.h>
#include "mysetup.h"
#include "mymenu.h"
#include "mytools.h"

#if defined(VDRVERSNUM)
#  if VDRVERSNUM < 20309
#    error "VDR-2.3.9 or greater is required!"
#  endif
#endif

using namespace std;

static const char *VERSION        = "2.0.10";
static const char *DESCRIPTION    = tr("Extended recordings menu");
static const char *MAINMENUENTRY  = "ExtRecMenuNG";

// --- cPluginExtrecmenu ------------------------------------------------------
class cPluginExtrecmenung:public cPlugin
{
  private:
  public:
    cPluginExtrecmenung(void);
    virtual ~cPluginExtrecmenung();
    virtual const char *Version(void){return VERSION;}
    virtual const char *Description(void){return tr(DESCRIPTION);}
    virtual const char *CommandLineHelp(void);
    virtual bool ProcessArgs(int argc,char *argv[]);
    virtual bool Initialize(void);
    virtual bool Start(void);
    virtual void Stop(void);
    virtual void Housekeeping(void);
    virtual cString Active(void);
    virtual const char *MainMenuEntry(void){return mysetup.HideMainMenuEntry ? NULL : MAINMENUENTRY;}
    virtual cOsdObject *MainMenuAction(void);
    virtual cMenuSetupPage *SetupMenu(void);
    virtual bool SetupParse(const char *Name,const char *Value);
    virtual bool Service(const char *Id,void *Data = NULL);
    virtual const char **SVDRPHelpPages(void);
    virtual cString SVDRPCommand(const char *Command, const char *Option, int &ReplyCode);
};

cPluginExtrecmenung::cPluginExtrecmenung(void)
{
}

cPluginExtrecmenung::~cPluginExtrecmenung()
{
}

const char *cPluginExtrecmenung::CommandLineHelp(void)
{
  return NULL;
}

bool cPluginExtrecmenung::ProcessArgs(int /* argc */, char ** /* argv */)
{
  return true;
}

bool cPluginExtrecmenung::Initialize(void)
{
  return true;
}

bool cPluginExtrecmenung::Start(void)
{
  Icons::InitCharSet();
  RecordingDirCommands.Load(AddDirectory(cPlugin::ConfigDirectory(PLUGIN_NAME_I18N), "dircmds.conf"));

  return true;
}

void cPluginExtrecmenung::Stop(void)
{
}

void cPluginExtrecmenung::Housekeeping(void)
{
}

cString cPluginExtrecmenung::Active(void)
{
  return NULL;
}

cOsdObject *cPluginExtrecmenung::MainMenuAction(void)
{
  return new myMenuRecordings();
}

cMenuSetupPage *cPluginExtrecmenung::SetupMenu(void)
{
  return new myMenuSetup();
}

bool cPluginExtrecmenung::SetupParse(const char *Name, const char *Value)
{
  return mysetup.SetupParse(Name, Value);
}

bool cPluginExtrecmenung::Service(const char *Id,void *Data)
{
  if (!Id)
    return false;
  if (mysetup.ReplaceOrgRecMenu && strcmp(Id, "MainMenuHooksPatch-v1.0::osRecordings") == 0) {
     if (!Data)
        return true;
     cOsdMenu **menu = (cOsdMenu**)Data;
     if (menu)
        *menu = (cOsdMenu*)MainMenuAction();
     return true;
     }
  return false;
}

const char **cPluginExtrecmenung::SVDRPHelpPages(void)
{
 return NULL;
}

cString cPluginExtrecmenung::SVDRPCommand(const char * /* Command */,const char * /* Option */,int & /* ReplyCode */)
{
 return NULL;
}

VDRPLUGINCREATOR(cPluginExtrecmenung); // Don't touch this!
